﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SkyCar
{
    [TestClass]
    public class TestJsonConnector
    {
        [TestMethod]
        public void JsonConnector_Write_Read_Success()
        {
            //given
            JsonConnector jsonConnector = new JsonConnector();
            UserSettings expectedProductInJsonFormat = new UserSettings(0, 0, 641, 466);
            UserSettings expectedJson = expectedProductInJsonFormat;
            UserSettings actualJson;

            //when
            JsonConnector.Write("test", expectedProductInJsonFormat);
            actualJson = jsonConnector.Read("test");

            //then
            Assert.ReferenceEquals(expectedJson, actualJson);
        }
    }
}