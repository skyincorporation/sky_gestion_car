﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace SkyCar
{
    /// <summary>
    /// Summary description for TestDbConnector
    /// </summary>
    [TestClass]
    public class TestDbConnector
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DbConnect_Exception_Failed()
        {
            DbConnector cnn = new DbConnector();
            cnn.DbConnect();
        }
    }
}