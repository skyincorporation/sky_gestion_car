﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SkyCar
{
    [TestClass]
    public class TestLogin
    {
        [TestMethod]
        public void UserManager_Login_Success()
        {
            //Ce test est là pour voir si le login passe avec success
            //given
            string userEmailAddress = "test@test.ch";
            string userPsw = "test";
            bool actualStringMessage;
            bool expectedStringMessage = true;

            UserManager usermanager = new UserManager();

            //when
            actualStringMessage = usermanager.IsLoginCorrect(userEmailAddress, userPsw);

            //then
            Assert.AreEqual(expectedStringMessage, actualStringMessage);
        }

        [TestMethod]
        public void UserManager_Register_Success()
        {
            //Ce test est là pour voir si le login passe avec success
            //given
            string userEmailAddress = "test2@test.ch";
            string userPsw = "test";
            bool actualStringMessage;
            bool expectedStringMessage = true;

            UserManager usermanager = new UserManager();

            //when
            actualStringMessage = usermanager.RegisterNewAccount(userEmailAddress, userPsw);

            //then
            Assert.AreEqual(expectedStringMessage, actualStringMessage);
        }
    }
}