﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SkyCar
{
    /// <summary>
    /// Summary description for TestCarManager
    /// </summary>
    [TestClass]
    public class TestCarManager
    {
        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion Additional test attributes

        [TestMethod]
        public void CarManager_AddtoDb_Success()
        {
            // given
            string model = "Skoda";
            string brend = "Fabia";
            string typefuel = "Essence";
            string power = "95";
            string nbChassis = "216468";
            string picture = null;

            bool actualStringMessage = false;
            bool expectedStringMessage = true;

            CarManager carmanager = new CarManager();
            Car car = new Car(model, brend, typefuel, power, nbChassis, picture);
            // when

            List<Car> list = car.CarList(car);
            actualStringMessage = carmanager.AddCar(list);

            // then
            Assert.AreEqual(expectedStringMessage, actualStringMessage);
        }

        //Ce Test sert à controler la valeur de la liste via la base de donnée
        [TestMethod]
        public void CarManager_SelectCar_Success()
        {
            // given
            List<Car> listCar = null;
            int nbChassis = 216468;
            string expectedStringMessage = "" + nbChassis;
            string actualStringMessage = "";
            CarManager carmanager = new CarManager();

            // when
            listCar = carmanager.SelectCar(nbChassis);

            actualStringMessage = listCar[0].NbChassis;

            // then
            Assert.AreEqual(expectedStringMessage, actualStringMessage);
        }
    }
}