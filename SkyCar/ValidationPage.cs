﻿using System;
using System.Windows.Forms;

namespace SkyCar
{
    public partial class frm_validate : Form
    {
        public frm_validate(string mail)
        {
            InitializeComponent();
            lbl_valid_msg.Text = "Merci de votre inscription " + mail;
        }

        private void Btn_log_login_Click(object sender, EventArgs e)
        {
            this.Hide();
            frm_login frm_Login = new frm_login();
            frm_Login.ShowDialog();
            this.Close();
        }
    }
}