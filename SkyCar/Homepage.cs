﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SkyCar
{
    public partial class frm_homePage : Form
    {
        public frm_homePage()
        {
            InitializeComponent();
            
            
        }

        private void Btn_homepage_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_home_add_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddCar frm_add = new AddCar();
            frm_add.ShowDialog();
            this.Close();
        }

        private void frm_homePage_Load(object sender, EventArgs e)
        {
            int i = 0;
            CarManager carmanager = new CarManager();
            List<Car> carList = carmanager.SelectCar();

            

            if(carList.Count >= 1)
            {
                groupBox2.Visible = true;

                groupBox2.Text = carList[0 + i].Model + " - " + carList[0 + i].Brend;
                picBox_1.ImageLocation = carList[0 + i].Picture;
                lbl_brand1.Text = carList[0 + i].Brend;
                lbl_gaz1.Text = carList[0 + i].Typefuel;
                lbl_power1.Text = carList[0 + i].Power;
                lbl_model1.Text = carList[0 + i].Model;
            }
            else
            {
                groupBox2.Visible = false;
            }

            if (carList.Count >= 2)
            {
                groupBox3.Visible = true;

                groupBox3.Text = carList[1 + i].Model + " - " + carList[1 + i].Brend;
                picBox2.ImageLocation = carList[1 + i].Picture;
                lbl_brand2.Text = carList[1 + i].Brend;
                lbl_gaz2.Text = carList[1 + i].Typefuel;
                lbl_power2.Text = carList[1 + i].Power;
                lbl_model2.Text = carList[1 + i].Model;
            }
            else
            {
                groupBox3.Visible = false;
            }

            if (carList.Count >= 3)
            {
                groupBox4.Visible = true;

                groupBox4.Text = carList[2 + i].Model + " - " + carList[2 + i].Brend;
                picBox3.ImageLocation = carList[2 + i].Picture;
                lbl_brand3.Text = carList[2 + i].Brend;
                lbl_gaz3.Text = carList[2 + i].Typefuel;
                lbl_power3.Text = carList[2 + i].Power;
                lbl_model3.Text = carList[2 + i].Model;
            }
            else
            {
                groupBox4.Visible = false;
            }

            if (carList.Count >= 4)
            {
                groupBox5.Visible = true;

                groupBox5.Text = carList[3 + i].Model + " - " + carList[3 + i].Brend;
                picBox4.ImageLocation = carList[3 + i].Picture;
                lbl_brand4.Text = carList[3 + i].Brend;
                lbl_gaz4.Text = carList[3 + i].Typefuel;
                lbl_power4.Text = carList[3 + i].Power;
                lbl_model4.Text = carList[3 + i].Model;
            }
            else
            {
                groupBox5.Visible = false;
            }

            if (carList.Count >= 5)
            {
                groupBox6.Visible = true;

                groupBox6.Text = carList[4 + i].Model + " - " + carList[4 + i].Brend;
                picBox5.ImageLocation = carList[4 + i].Picture;
                lbl_brand5.Text = carList[4 + i].Brend;
                lbl_gaz5.Text = carList[4 + i].Typefuel;
                lbl_power5.Text = carList[4 + i].Power;
                lbl_model5.Text = carList[4 + i].Model;
            }
            else
            {
                groupBox6.Visible = false;
            }

            if (carList.Count >= 6)
            {
                groupBox7.Visible = true;

                groupBox7.Text = carList[5 + i].Model + " - " + carList[5 + i].Brend;
                picBox6.ImageLocation = carList[5 + i].Picture;
                lbl_brand6.Text = carList[5 + i].Brend;
                lbl_gaz6.Text = carList[5 + i].Typefuel;
                lbl_power6.Text = carList[5 + i].Power;
                lbl_model6.Text = carList[5 + i].Model;
            }
            else
            {
                groupBox7.Visible = false;
            }
        }
    }
}