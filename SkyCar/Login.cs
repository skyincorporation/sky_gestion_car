﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace SkyCar
{
    public partial class frm_login : Form
    {
        private string currentPassword = "";

        public frm_login()
        {
            InitializeComponent();
        }

        private void linklbl_log_goregister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            frm_register frm_Register = new frm_register();
            frm_Register.ShowDialog();
            this.Close();
        }

        private void btn_log_login_Click(object sender, EventArgs e)
        {
            string email = txtbox_log_mail.Text;

            bool emailNotVaild = IsValidEmail(email);

            if (string.IsNullOrEmpty(email) || emailNotVaild == false)
            {
                MessageBox.Show("L'adresse E-mail n'ai pas rempli correctement", "Login Incomplet ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (string.IsNullOrEmpty(currentPassword))
            {
                MessageBox.Show("Le Mot de passe n'ai pas rempli correctement", "Login Incomplet ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                UserManager userManager = new UserManager();
                try
                {
                    bool loginCorrect = userManager.IsLoginCorrect(email, currentPassword);
                    if (loginCorrect)
                    {
                        this.Hide();
                        Session.Email = email;
                        frm_homePage frm_homePage = new frm_homePage();
                        frm_homePage.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("L'E-mail ou le mot de passe est incorrect", "Login Incorrect", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Du à un probléme avec notre serveur, le service est momentanément indisponible", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void txtbox_log_mdp_TextChanged(object sender, EventArgs e)
        {
            TextBox currentInputBox = sender as TextBox;
            var inputString = currentInputBox.Text;

            Time_PSW.Enabled = true;
            Time_PSW.Stop();
            Time_PSW.Start();
            if (inputString.Length > currentPassword.Length)
            {
                var newChar = inputString.Substring(inputString.Length - 1);
                currentPassword += newChar;
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length - 1; i++)
                {
                    newBoxText += "*";
                }
                newBoxText += newChar;
                currentInputBox.Text = newBoxText;
                txtbox_log_mdp.SelectionStart = txtbox_log_mdp.Text.Count();
            }
            else if ((inputString.Length < currentPassword.Length) && (inputString.Length > 0)) // Removes characters from the currentPassword field til it matches the length of the textbox field and then exposes a single character at the end
            {
                currentPassword = currentPassword.Remove(inputString.Length, currentPassword.Length - inputString.Length);
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length; i++)
                {
                    newBoxText += "*";
                }
                currentInputBox.Text = newBoxText;
                txtbox_log_mdp.SelectionStart = txtbox_log_mdp.Text.Count();
            }
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void Time_PSW_Tick(object sender, EventArgs e)
        {
            string text = "";
            for (int i = 0; i < txtbox_log_mdp.Text.Count(); i++)
            {
                text = text + "*";
            }
            txtbox_log_mdp.Text = text;
            Time_PSW.Enabled = false;
            txtbox_log_mdp.SelectionStart = txtbox_log_mdp.Text.Count();
        }
    }
}