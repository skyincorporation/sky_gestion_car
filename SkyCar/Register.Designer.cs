﻿namespace SkyCar
{
    partial class frm_register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_reg_register = new System.Windows.Forms.Button();
            this.txtbox_reg_mdp = new System.Windows.Forms.TextBox();
            this.txtbox_reg_mail = new System.Windows.Forms.TextBox();
            this.grpbox_log_login = new System.Windows.Forms.GroupBox();
            this.linklbl_reg_login = new System.Windows.Forms.LinkLabel();
            this.txtbox_confirmmdp = new System.Windows.Forms.TextBox();
            this.lbl_reg_mdp = new System.Windows.Forms.Label();
            this.lbl_reg_confmdp = new System.Windows.Forms.Label();
            this.lbl_reg_login = new System.Windows.Forms.Label();
            this.lbl_reg_mail = new System.Windows.Forms.Label();
            this.picbox_reg_logo = new System.Windows.Forms.PictureBox();
            this.TimePSW = new System.Windows.Forms.Timer(this.components);
            this.TimePSW2 = new System.Windows.Forms.Timer(this.components);
            this.grpbox_log_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox_reg_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_reg_register
            // 
            this.btn_reg_register.BackColor = System.Drawing.Color.Transparent;
            this.btn_reg_register.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_reg_register.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_reg_register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_reg_register.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_reg_register.FlatAppearance.BorderSize = 0;
            this.btn_reg_register.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reg_register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reg_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reg_register.ForeColor = System.Drawing.Color.Transparent;
            this.btn_reg_register.Location = new System.Drawing.Point(58, 386);
            this.btn_reg_register.Name = "btn_reg_register";
            this.btn_reg_register.Size = new System.Drawing.Size(250, 49);
            this.btn_reg_register.TabIndex = 11;
            this.btn_reg_register.Text = "S\'enregistrer";
            this.btn_reg_register.UseVisualStyleBackColor = false;
            this.btn_reg_register.Click += new System.EventHandler(this.btn_reg_register_Click);
            // 
            // txtbox_reg_mdp
            // 
            this.txtbox_reg_mdp.BackColor = System.Drawing.Color.AliceBlue;
            this.txtbox_reg_mdp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_reg_mdp.Location = new System.Drawing.Point(72, 255);
            this.txtbox_reg_mdp.Name = "txtbox_reg_mdp";
            this.txtbox_reg_mdp.Size = new System.Drawing.Size(232, 26);
            this.txtbox_reg_mdp.TabIndex = 9;
            this.txtbox_reg_mdp.TextChanged += new System.EventHandler(this.txtbox_reg_mdp_TextChanged);
            // 
            // txtbox_reg_mail
            // 
            this.txtbox_reg_mail.BackColor = System.Drawing.Color.AliceBlue;
            this.txtbox_reg_mail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_reg_mail.Location = new System.Drawing.Point(72, 198);
            this.txtbox_reg_mail.Name = "txtbox_reg_mail";
            this.txtbox_reg_mail.Size = new System.Drawing.Size(232, 26);
            this.txtbox_reg_mail.TabIndex = 8;
            // 
            // grpbox_log_login
            // 
            this.grpbox_log_login.BackColor = System.Drawing.Color.LightSkyBlue;
            this.grpbox_log_login.Controls.Add(this.linklbl_reg_login);
            this.grpbox_log_login.Controls.Add(this.txtbox_confirmmdp);
            this.grpbox_log_login.Controls.Add(this.lbl_reg_mdp);
            this.grpbox_log_login.Controls.Add(this.lbl_reg_confmdp);
            this.grpbox_log_login.Controls.Add(this.lbl_reg_login);
            this.grpbox_log_login.Controls.Add(this.lbl_reg_mail);
            this.grpbox_log_login.Location = new System.Drawing.Point(58, 123);
            this.grpbox_log_login.Name = "grpbox_log_login";
            this.grpbox_log_login.Size = new System.Drawing.Size(256, 257);
            this.grpbox_log_login.TabIndex = 10;
            this.grpbox_log_login.TabStop = false;
            // 
            // linklbl_reg_login
            // 
            this.linklbl_reg_login.AutoSize = true;
            this.linklbl_reg_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linklbl_reg_login.LinkColor = System.Drawing.Color.Black;
            this.linklbl_reg_login.Location = new System.Drawing.Point(11, 228);
            this.linklbl_reg_login.Name = "linklbl_reg_login";
            this.linklbl_reg_login.Size = new System.Drawing.Size(199, 17);
            this.linklbl_reg_login.TabIndex = 12;
            this.linklbl_reg_login.TabStop = true;
            this.linklbl_reg_login.Text = "Inscris ? Connectez-vous !";
            this.linklbl_reg_login.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Linklbl_reg_login_LinkClicked);
            // 
            // txtbox_confirmmdp
            // 
            this.txtbox_confirmmdp.BackColor = System.Drawing.Color.AliceBlue;
            this.txtbox_confirmmdp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_confirmmdp.Location = new System.Drawing.Point(14, 189);
            this.txtbox_confirmmdp.Name = "txtbox_confirmmdp";
            this.txtbox_confirmmdp.Size = new System.Drawing.Size(232, 26);
            this.txtbox_confirmmdp.TabIndex = 11;
            this.txtbox_confirmmdp.TextChanged += new System.EventHandler(this.txtbox_confirmmdp_TextChanged);
            // 
            // lbl_reg_mdp
            // 
            this.lbl_reg_mdp.AutoSize = true;
            this.lbl_reg_mdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reg_mdp.ForeColor = System.Drawing.Color.White;
            this.lbl_reg_mdp.Location = new System.Drawing.Point(8, 105);
            this.lbl_reg_mdp.Name = "lbl_reg_mdp";
            this.lbl_reg_mdp.Size = new System.Drawing.Size(142, 25);
            this.lbl_reg_mdp.TabIndex = 5;
            this.lbl_reg_mdp.Text = "Mot de passe";
            // 
            // lbl_reg_confmdp
            // 
            this.lbl_reg_confmdp.AutoSize = true;
            this.lbl_reg_confmdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reg_confmdp.ForeColor = System.Drawing.Color.White;
            this.lbl_reg_confmdp.Location = new System.Drawing.Point(8, 162);
            this.lbl_reg_confmdp.Name = "lbl_reg_confmdp";
            this.lbl_reg_confmdp.Size = new System.Drawing.Size(241, 25);
            this.lbl_reg_confmdp.TabIndex = 10;
            this.lbl_reg_confmdp.Text = "Confirmer mot de passe";
            // 
            // lbl_reg_login
            // 
            this.lbl_reg_login.AutoSize = true;
            this.lbl_reg_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reg_login.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_reg_login.Location = new System.Drawing.Point(34, 0);
            this.lbl_reg_login.Name = "lbl_reg_login";
            this.lbl_reg_login.Size = new System.Drawing.Size(174, 37);
            this.lbl_reg_login.TabIndex = 0;
            this.lbl_reg_login.Text = "Inscription";
            this.lbl_reg_login.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_reg_mail
            // 
            this.lbl_reg_mail.AutoSize = true;
            this.lbl_reg_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reg_mail.ForeColor = System.Drawing.Color.White;
            this.lbl_reg_mail.Location = new System.Drawing.Point(8, 48);
            this.lbl_reg_mail.Name = "lbl_reg_mail";
            this.lbl_reg_mail.Size = new System.Drawing.Size(157, 25);
            this.lbl_reg_mail.TabIndex = 4;
            this.lbl_reg_mail.Text = "Adresse e-mail";
            // 
            // picbox_reg_logo
            // 
            this.picbox_reg_logo.Dock = System.Windows.Forms.DockStyle.Top;
            this.picbox_reg_logo.Image = global::SkyCar.Properties.Resources._1200px_SKY_Basic_Logo;
            this.picbox_reg_logo.Location = new System.Drawing.Point(0, 0);
            this.picbox_reg_logo.Name = "picbox_reg_logo";
            this.picbox_reg_logo.Size = new System.Drawing.Size(378, 114);
            this.picbox_reg_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picbox_reg_logo.TabIndex = 0;
            this.picbox_reg_logo.TabStop = false;
            // 
            // TimePSW
            // 
            this.TimePSW.Interval = 1000;
            this.TimePSW.Tick += new System.EventHandler(this.TimePSW_Tick);
            // 
            // TimePSW2
            // 
            this.TimePSW2.Interval = 1000;
            this.TimePSW2.Tick += new System.EventHandler(this.TimePSW2_Tick);
            // 
            // frm_register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(378, 445);
            this.Controls.Add(this.btn_reg_register);
            this.Controls.Add(this.txtbox_reg_mdp);
            this.Controls.Add(this.txtbox_reg_mail);
            this.Controls.Add(this.grpbox_log_login);
            this.Controls.Add(this.picbox_reg_logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Register";
            this.grpbox_log_login.ResumeLayout(false);
            this.grpbox_log_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox_reg_logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picbox_reg_logo;
        private System.Windows.Forms.Button btn_reg_register;
        private System.Windows.Forms.TextBox txtbox_reg_mdp;
        private System.Windows.Forms.TextBox txtbox_reg_mail;
        private System.Windows.Forms.GroupBox grpbox_log_login;
        private System.Windows.Forms.TextBox txtbox_confirmmdp;
        private System.Windows.Forms.Label lbl_reg_mdp;
        private System.Windows.Forms.Label lbl_reg_confmdp;
        private System.Windows.Forms.Label lbl_reg_login;
        private System.Windows.Forms.Label lbl_reg_mail;
        private System.Windows.Forms.LinkLabel linklbl_reg_login;
        private System.Windows.Forms.Timer TimePSW;
        private System.Windows.Forms.Timer TimePSW2;
    }
}