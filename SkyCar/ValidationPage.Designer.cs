﻿namespace SkyCar
{
    partial class frm_validate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_valid_msg = new System.Windows.Forms.Label();
            this.btn_log_login = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::SkyCar.Properties.Resources._1200px_SKY_Basic_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(610, 125);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lbl_valid_msg
            // 
            this.lbl_valid_msg.AutoSize = true;
            this.lbl_valid_msg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_valid_msg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_valid_msg.ForeColor = System.Drawing.Color.White;
            this.lbl_valid_msg.Location = new System.Drawing.Point(0, 125);
            this.lbl_valid_msg.Name = "lbl_valid_msg";
            this.lbl_valid_msg.Size = new System.Drawing.Size(125, 26);
            this.lbl_valid_msg.TabIndex = 1;
            this.lbl_valid_msg.Text = "ValidInscr.";
            // 
            // btn_log_login
            // 
            this.btn_log_login.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_log_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_log_login.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_log_login.FlatAppearance.BorderSize = 0;
            this.btn_log_login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
            this.btn_log_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_log_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_log_login.ForeColor = System.Drawing.Color.Transparent;
            this.btn_log_login.Location = new System.Drawing.Point(188, 177);
            this.btn_log_login.Name = "btn_log_login";
            this.btn_log_login.Size = new System.Drawing.Size(231, 37);
            this.btn_log_login.TabIndex = 8;
            this.btn_log_login.Text = "Se connecter";
            this.btn_log_login.UseVisualStyleBackColor = false;
            this.btn_log_login.Click += new System.EventHandler(this.Btn_log_login_Click);
            // 
            // frm_validate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(610, 226);
            this.Controls.Add(this.btn_log_login);
            this.Controls.Add(this.lbl_valid_msg);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frm_validate";
            this.Text = "ValidationPage";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_valid_msg;
        private System.Windows.Forms.Button btn_log_login;
    }
}