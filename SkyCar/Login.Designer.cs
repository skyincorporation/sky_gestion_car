﻿namespace SkyCar
{
    partial class frm_login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_log_login = new System.Windows.Forms.Label();
            this.txtbox_log_mail = new System.Windows.Forms.TextBox();
            this.txtbox_log_mdp = new System.Windows.Forms.TextBox();
            this.lbl_log_mail = new System.Windows.Forms.Label();
            this.lbl_log_mdp = new System.Windows.Forms.Label();
            this.grpbox_log_login = new System.Windows.Forms.GroupBox();
            this.linklbl_log_goregister = new System.Windows.Forms.LinkLabel();
            this.btn_log_login = new System.Windows.Forms.Button();
            this.Time_PSW = new System.Windows.Forms.Timer(this.components);
            this.picbox_log_sky = new System.Windows.Forms.PictureBox();
            this.grpbox_log_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox_log_sky)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_log_login
            // 
            this.lbl_log_login.AutoSize = true;
            this.lbl_log_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_log_login.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_log_login.Location = new System.Drawing.Point(34, 0);
            this.lbl_log_login.Name = "lbl_log_login";
            this.lbl_log_login.Size = new System.Drawing.Size(178, 37);
            this.lbl_log_login.TabIndex = 0;
            this.lbl_log_login.Text = "Connexion";
            this.lbl_log_login.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtbox_log_mail
            // 
            this.txtbox_log_mail.BackColor = System.Drawing.Color.AliceBlue;
            this.txtbox_log_mail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_log_mail.Location = new System.Drawing.Point(70, 225);
            this.txtbox_log_mail.Name = "txtbox_log_mail";
            this.txtbox_log_mail.Size = new System.Drawing.Size(232, 26);
            this.txtbox_log_mail.TabIndex = 2;
            // 
            // txtbox_log_mdp
            // 
            this.txtbox_log_mdp.BackColor = System.Drawing.Color.AliceBlue;
            this.txtbox_log_mdp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_log_mdp.Location = new System.Drawing.Point(70, 280);
            this.txtbox_log_mdp.Name = "txtbox_log_mdp";
            this.txtbox_log_mdp.Size = new System.Drawing.Size(232, 26);
            this.txtbox_log_mdp.TabIndex = 3;
            this.txtbox_log_mdp.TextChanged += new System.EventHandler(this.txtbox_log_mdp_TextChanged);
            // 
            // lbl_log_mail
            // 
            this.lbl_log_mail.AutoSize = true;
            this.lbl_log_mail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_log_mail.ForeColor = System.Drawing.Color.White;
            this.lbl_log_mail.Location = new System.Drawing.Point(8, 48);
            this.lbl_log_mail.Name = "lbl_log_mail";
            this.lbl_log_mail.Size = new System.Drawing.Size(157, 25);
            this.lbl_log_mail.TabIndex = 4;
            this.lbl_log_mail.Text = "Adresse e-mail";
            // 
            // lbl_log_mdp
            // 
            this.lbl_log_mdp.AutoSize = true;
            this.lbl_log_mdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_log_mdp.ForeColor = System.Drawing.Color.White;
            this.lbl_log_mdp.Location = new System.Drawing.Point(8, 105);
            this.lbl_log_mdp.Name = "lbl_log_mdp";
            this.lbl_log_mdp.Size = new System.Drawing.Size(142, 25);
            this.lbl_log_mdp.TabIndex = 5;
            this.lbl_log_mdp.Text = "Mot de passe";
            // 
            // grpbox_log_login
            // 
            this.grpbox_log_login.BackColor = System.Drawing.Color.LightSkyBlue;
            this.grpbox_log_login.Controls.Add(this.linklbl_log_goregister);
            this.grpbox_log_login.Controls.Add(this.lbl_log_mdp);
            this.grpbox_log_login.Controls.Add(this.lbl_log_login);
            this.grpbox_log_login.Controls.Add(this.lbl_log_mail);
            this.grpbox_log_login.Location = new System.Drawing.Point(57, 148);
            this.grpbox_log_login.Name = "grpbox_log_login";
            this.grpbox_log_login.Size = new System.Drawing.Size(256, 215);
            this.grpbox_log_login.TabIndex = 6;
            this.grpbox_log_login.TabStop = false;
            // 
            // linklbl_log_goregister
            // 
            this.linklbl_log_goregister.ActiveLinkColor = System.Drawing.Color.LightSkyBlue;
            this.linklbl_log_goregister.AutoSize = true;
            this.linklbl_log_goregister.BackColor = System.Drawing.Color.LightSkyBlue;
            this.linklbl_log_goregister.Font = new System.Drawing.Font("Caviar Dreams", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linklbl_log_goregister.LinkColor = System.Drawing.Color.Black;
            this.linklbl_log_goregister.Location = new System.Drawing.Point(22, 175);
            this.linklbl_log_goregister.Name = "linklbl_log_goregister";
            this.linklbl_log_goregister.Size = new System.Drawing.Size(194, 21);
            this.linklbl_log_goregister.TabIndex = 0;
            this.linklbl_log_goregister.TabStop = true;
            this.linklbl_log_goregister.Text = "Pas encore enregistré ?";
            this.linklbl_log_goregister.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklbl_log_goregister_LinkClicked);
            // 
            // btn_log_login
            // 
            this.btn_log_login.BackColor = System.Drawing.Color.Transparent;
            this.btn_log_login.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_log_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_log_login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_log_login.FlatAppearance.BorderSize = 0;
            this.btn_log_login.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_log_login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_log_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_log_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_log_login.ForeColor = System.Drawing.Color.Transparent;
            this.btn_log_login.Location = new System.Drawing.Point(57, 369);
            this.btn_log_login.Name = "btn_log_login";
            this.btn_log_login.Size = new System.Drawing.Size(256, 47);
            this.btn_log_login.TabIndex = 7;
            this.btn_log_login.Text = "Se connecter";
            this.btn_log_login.UseVisualStyleBackColor = false;
            this.btn_log_login.Click += new System.EventHandler(this.btn_log_login_Click);
            // 
            // Time_PSW
            // 
            this.Time_PSW.Interval = 1000;
            this.Time_PSW.Tick += new System.EventHandler(this.Time_PSW_Tick);
            // 
            // picbox_log_sky
            // 
            this.picbox_log_sky.Image = global::SkyCar.Properties.Resources._1200px_SKY_Basic_Logo;
            this.picbox_log_sky.Location = new System.Drawing.Point(12, 12);
            this.picbox_log_sky.Name = "picbox_log_sky";
            this.picbox_log_sky.Size = new System.Drawing.Size(352, 114);
            this.picbox_log_sky.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picbox_log_sky.TabIndex = 1;
            this.picbox_log_sky.TabStop = false;
            // 
            // frm_login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(378, 428);
            this.Controls.Add(this.btn_log_login);
            this.Controls.Add(this.txtbox_log_mdp);
            this.Controls.Add(this.txtbox_log_mail);
            this.Controls.Add(this.picbox_log_sky);
            this.Controls.Add(this.grpbox_log_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.grpbox_log_login.ResumeLayout(false);
            this.grpbox_log_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picbox_log_sky)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_log_login;
        private System.Windows.Forms.PictureBox picbox_log_sky;
        private System.Windows.Forms.TextBox txtbox_log_mail;
        private System.Windows.Forms.TextBox txtbox_log_mdp;
        private System.Windows.Forms.Label lbl_log_mail;
        private System.Windows.Forms.Label lbl_log_mdp;
        private System.Windows.Forms.GroupBox grpbox_log_login;
        private System.Windows.Forms.LinkLabel linklbl_log_goregister;
        private System.Windows.Forms.Button btn_log_login;
        private System.Windows.Forms.Timer Time_PSW;
    }
}

