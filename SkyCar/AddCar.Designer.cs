﻿namespace SkyCar
{
    partial class AddCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCar));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.grpBox_Addcar = new System.Windows.Forms.GroupBox();
            this.cmbBox_add_Gaz = new System.Windows.Forms.ComboBox();
            this.picBox_add_image = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBox_add_Image = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbox_add_number = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_add_del = new System.Windows.Forms.Button();
            this.btn_add_add = new System.Windows.Forms.Button();
            this.txtbox_add_Power = new System.Windows.Forms.TextBox();
            this.txtbox_add_Model = new System.Windows.Forms.TextBox();
            this.txtbox_add_Brand = new System.Windows.Forms.TextBox();
            this.btn_add_menu_home = new System.Windows.Forms.Button();
            this.btn_add_menu_exit = new System.Windows.Forms.Button();
            this.btn_add_menu_list = new System.Windows.Forms.Button();
            this.btn_add_menu_del = new System.Windows.Forms.Button();
            this.btn_add_menu_preview = new System.Windows.Forms.Button();
            this.btn_add_menu_next = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpBox_Addcar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_add_image)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SkyCar.Properties.Resources._1200px_SKY_Basic_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 165);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // grpBox_Addcar
            // 
            this.grpBox_Addcar.Controls.Add(this.cmbBox_add_Gaz);
            this.grpBox_Addcar.Controls.Add(this.picBox_add_image);
            this.grpBox_Addcar.Controls.Add(this.label6);
            this.grpBox_Addcar.Controls.Add(this.txtBox_add_Image);
            this.grpBox_Addcar.Controls.Add(this.label5);
            this.grpBox_Addcar.Controls.Add(this.txtbox_add_number);
            this.grpBox_Addcar.Controls.Add(this.label4);
            this.grpBox_Addcar.Controls.Add(this.label3);
            this.grpBox_Addcar.Controls.Add(this.label2);
            this.grpBox_Addcar.Controls.Add(this.label1);
            this.grpBox_Addcar.Controls.Add(this.btn_add_del);
            this.grpBox_Addcar.Controls.Add(this.btn_add_add);
            this.grpBox_Addcar.Controls.Add(this.txtbox_add_Power);
            this.grpBox_Addcar.Controls.Add(this.txtbox_add_Model);
            this.grpBox_Addcar.Controls.Add(this.txtbox_add_Brand);
            this.grpBox_Addcar.Location = new System.Drawing.Point(282, 64);
            this.grpBox_Addcar.Name = "grpBox_Addcar";
            this.grpBox_Addcar.Size = new System.Drawing.Size(401, 554);
            this.grpBox_Addcar.TabIndex = 15;
            this.grpBox_Addcar.TabStop = false;
            this.grpBox_Addcar.Text = "Ajout d\'une voiture";
            // 
            // cmbBox_add_Gaz
            // 
            this.cmbBox_add_Gaz.FormattingEnabled = true;
            this.cmbBox_add_Gaz.Items.AddRange(new object[] {
            "Essence",
            "Diesel",
            "Bio-Ethanol",
            "Electrique",
            "Hybride"});
            this.cmbBox_add_Gaz.Location = new System.Drawing.Point(48, 396);
            this.cmbBox_add_Gaz.Name = "cmbBox_add_Gaz";
            this.cmbBox_add_Gaz.Size = new System.Drawing.Size(293, 28);
            this.cmbBox_add_Gaz.TabIndex = 5;
            // 
            // picBox_add_image
            // 
            this.picBox_add_image.Location = new System.Drawing.Point(48, 37);
            this.picBox_add_image.Name = "picBox_add_image";
            this.picBox_add_image.Size = new System.Drawing.Size(293, 110);
            this.picBox_add_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox_add_image.TabIndex = 19;
            this.picBox_add_image.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(155, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "Image";
            // 
            // txtBox_add_Image
            // 
            this.txtBox_add_Image.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtBox_add_Image.Location = new System.Drawing.Point(48, 173);
            this.txtBox_add_Image.Name = "txtBox_add_Image";
            this.txtBox_add_Image.Size = new System.Drawing.Size(293, 26);
            this.txtBox_add_Image.TabIndex = 17;
            this.txtBox_add_Image.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtBox_add_Image_MouseClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(155, 426);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "N° de chassis";
            // 
            // txtbox_add_number
            // 
            this.txtbox_add_number.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtbox_add_number.Location = new System.Drawing.Point(48, 456);
            this.txtbox_add_number.MaxLength = 6;
            this.txtbox_add_number.Name = "txtbox_add_number";
            this.txtbox_add_number.Size = new System.Drawing.Size(293, 26);
            this.txtbox_add_number.TabIndex = 6;
            this.txtbox_add_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txtbox_add_number_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(155, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "Marque";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(155, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 20);
            this.label3.TabIndex = 13;
            this.label3.Text = "Modèle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(155, 317);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 12;
            this.label2.Text = "Puissance";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(155, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Carburant";
            // 
            // btn_add_del
            // 
            this.btn_add_del.Location = new System.Drawing.Point(214, 504);
            this.btn_add_del.Name = "btn_add_del";
            this.btn_add_del.Size = new System.Drawing.Size(127, 30);
            this.btn_add_del.TabIndex = 7;
            this.btn_add_del.Text = "Effacer";
            this.btn_add_del.UseVisualStyleBackColor = true;
            this.btn_add_del.Click += new System.EventHandler(this.Btn_add_del_Click);
            // 
            // btn_add_add
            // 
            this.btn_add_add.Location = new System.Drawing.Point(48, 504);
            this.btn_add_add.Name = "btn_add_add";
            this.btn_add_add.Size = new System.Drawing.Size(127, 30);
            this.btn_add_add.TabIndex = 6;
            this.btn_add_add.Text = "Ajouter";
            this.btn_add_add.UseVisualStyleBackColor = true;
            this.btn_add_add.Click += new System.EventHandler(this.Btn_add_add_Click);
            // 
            // txtbox_add_Power
            // 
            this.txtbox_add_Power.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtbox_add_Power.Location = new System.Drawing.Point(48, 340);
            this.txtbox_add_Power.MaxLength = 4;
            this.txtbox_add_Power.Name = "txtbox_add_Power";
            this.txtbox_add_Power.Size = new System.Drawing.Size(293, 26);
            this.txtbox_add_Power.TabIndex = 4;
            this.txtbox_add_Power.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txtbox_add_Power_KeyPress);
            // 
            // txtbox_add_Model
            // 
            this.txtbox_add_Model.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtbox_add_Model.Location = new System.Drawing.Point(48, 287);
            this.txtbox_add_Model.Name = "txtbox_add_Model";
            this.txtbox_add_Model.Size = new System.Drawing.Size(293, 26);
            this.txtbox_add_Model.TabIndex = 3;
            // 
            // txtbox_add_Brand
            // 
            this.txtbox_add_Brand.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtbox_add_Brand.Location = new System.Drawing.Point(48, 225);
            this.txtbox_add_Brand.Name = "txtbox_add_Brand";
            this.txtbox_add_Brand.Size = new System.Drawing.Size(293, 26);
            this.txtbox_add_Brand.TabIndex = 2;
            // 
            // btn_add_menu_home
            // 
            this.btn_add_menu_home.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_home.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_add_menu_home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_add_menu_home.FlatAppearance.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_home.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_home.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_add_menu_home.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_add_menu_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_home.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_home.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_home.Location = new System.Drawing.Point(12, 183);
            this.btn_add_menu_home.Name = "btn_add_menu_home";
            this.btn_add_menu_home.Size = new System.Drawing.Size(200, 97);
            this.btn_add_menu_home.TabIndex = 15;
            this.btn_add_menu_home.Text = "Accueil";
            this.btn_add_menu_home.UseVisualStyleBackColor = false;
            this.btn_add_menu_home.Click += new System.EventHandler(this.Btn_add_menu_home_Click);
            // 
            // btn_add_menu_exit
            // 
            this.btn_add_menu_exit.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_exit.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_add_menu_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_add_menu_exit.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_add_menu_exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_add_menu_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_exit.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_exit.Location = new System.Drawing.Point(12, 566);
            this.btn_add_menu_exit.Name = "btn_add_menu_exit";
            this.btn_add_menu_exit.Size = new System.Drawing.Size(200, 62);
            this.btn_add_menu_exit.TabIndex = 20;
            this.btn_add_menu_exit.Text = "Quitter";
            this.btn_add_menu_exit.UseVisualStyleBackColor = false;
            this.btn_add_menu_exit.Click += new System.EventHandler(this.Btn_add_menu_exit_Click);
            // 
            // btn_add_menu_list
            // 
            this.btn_add_menu_list.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_list.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_add_menu_list.BackgroundImage")));
            this.btn_add_menu_list.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_list.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_list.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_list.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_list.Location = new System.Drawing.Point(12, 348);
            this.btn_add_menu_list.Name = "btn_add_menu_list";
            this.btn_add_menu_list.Size = new System.Drawing.Size(200, 97);
            this.btn_add_menu_list.TabIndex = 17;
            this.btn_add_menu_list.Text = "Liste simple";
            this.btn_add_menu_list.UseVisualStyleBackColor = false;
            // 
            // btn_add_menu_del
            // 
            this.btn_add_menu_del.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_add_menu_del.BackgroundImage")));
            this.btn_add_menu_del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_del.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_del.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_del.Location = new System.Drawing.Point(12, 264);
            this.btn_add_menu_del.Name = "btn_add_menu_del";
            this.btn_add_menu_del.Size = new System.Drawing.Size(200, 97);
            this.btn_add_menu_del.TabIndex = 16;
            this.btn_add_menu_del.Text = "Supprimer";
            this.btn_add_menu_del.UseVisualStyleBackColor = false;
            // 
            // btn_add_menu_preview
            // 
            this.btn_add_menu_preview.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_preview.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_add_menu_preview.BackgroundImage")));
            this.btn_add_menu_preview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_preview.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_preview.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_preview.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_preview.Location = new System.Drawing.Point(12, 493);
            this.btn_add_menu_preview.Name = "btn_add_menu_preview";
            this.btn_add_menu_preview.Size = new System.Drawing.Size(200, 45);
            this.btn_add_menu_preview.TabIndex = 19;
            this.btn_add_menu_preview.Text = "<----";
            this.btn_add_menu_preview.UseVisualStyleBackColor = false;
            // 
            // btn_add_menu_next
            // 
            this.btn_add_menu_next.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_add_menu_next.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_add_menu_next.BackgroundImage")));
            this.btn_add_menu_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_add_menu_next.FlatAppearance.BorderSize = 0;
            this.btn_add_menu_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add_menu_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add_menu_next.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_add_menu_next.Location = new System.Drawing.Point(12, 451);
            this.btn_add_menu_next.Name = "btn_add_menu_next";
            this.btn_add_menu_next.Size = new System.Drawing.Size(200, 45);
            this.btn_add_menu_next.TabIndex = 18;
            this.btn_add_menu_next.Text = "---->";
            this.btn_add_menu_next.UseVisualStyleBackColor = false;
            // 
            // AddCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(724, 653);
            this.Controls.Add(this.btn_add_menu_preview);
            this.Controls.Add(this.grpBox_Addcar);
            this.Controls.Add(this.btn_add_menu_next);
            this.Controls.Add(this.btn_add_menu_list);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_add_menu_del);
            this.Controls.Add(this.btn_add_menu_home);
            this.Controls.Add(this.btn_add_menu_exit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddCar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Car";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpBox_Addcar.ResumeLayout(false);
            this.grpBox_Addcar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_add_image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox grpBox_Addcar;
        private System.Windows.Forms.TextBox txtbox_add_Power;
        private System.Windows.Forms.TextBox txtbox_add_Model;
        private System.Windows.Forms.TextBox txtbox_add_Brand;
        private System.Windows.Forms.Button btn_add_del;
        private System.Windows.Forms.Button btn_add_add;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_add_menu_home;
        private System.Windows.Forms.Button btn_add_menu_exit;
        private System.Windows.Forms.Button btn_add_menu_list;
        private System.Windows.Forms.Button btn_add_menu_del;
        private System.Windows.Forms.Button btn_add_menu_preview;
        private System.Windows.Forms.Button btn_add_menu_next;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbox_add_number;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBox_add_Image;
        private System.Windows.Forms.PictureBox picBox_add_image;
        private System.Windows.Forms.ComboBox cmbBox_add_Gaz;
    }
}