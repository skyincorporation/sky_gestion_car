﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SkyCar
{
    public partial class frm_register : Form
    {
        private string currentPassword = "";

        public frm_register()
        {
            InitializeComponent();
        }

        private void Linklbl_reg_login_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            frm_login frm_Login = new frm_login();
            frm_Login.ShowDialog();
            this.Close();
        }

        private void btn_reg_register_Click(object sender, EventArgs e)
        {
            string password = currentPassword;
            string email = txtbox_reg_mail.Text;

            try
            {
                if (string.IsNullOrEmpty(txtbox_reg_mail.Text) || string.IsNullOrEmpty(txtbox_reg_mdp.Text) || string.IsNullOrEmpty(txtbox_confirmmdp.Text))
                {
                    MessageBox.Show("Champ(s) Manquant", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (IsValidEmail(email))
                    {
                        if (txtbox_reg_mdp.Text == txtbox_confirmmdp.Text)
                        {
                            if (password.Length >= 8)
                            {
                                Regex Passe = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$");

                                if (Passe.IsMatch(password.ToString()))
                                {
                                    UserManager userManager = new UserManager();

                                    bool registerCorrect = userManager.RegisterNewAccount(email, password);

                                    this.Hide();
                                    frm_validate frm_validate = new frm_validate(email);
                                    frm_validate.ShowDialog();
                                    this.Close();
                                }
                                else
                                {
                                    MessageBox.Show("Le mot de passe doit contenir: \n" + "Minimun un chiffre\n" + "Minimun une lettre minuscule\n" + "Minimun une lettre majuscule\n" + "Minimun un caractère spécial", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Mot de passe trop court (8 caractères minimum)", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Les mots de passes ne corespondent pas", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Email non valide", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (DbError)
            {
                MessageBox.Show("Du à un probléme avec notre serveur, le inscription sont temporairement désactivée", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void TimePSW_Tick(object sender, EventArgs e)
        {
            TextBox mdp = txtbox_reg_mdp;
            string text = "";
            for (int i = 0; i < mdp.Text.Count(); i++)
            {
                text = text + "*";
            }
            mdp.Text = text;
            TimePSW.Enabled = false;
            mdp.SelectionStart = mdp.Text.Count();
        }

        private void txtbox_reg_mdp_TextChanged(object sender, EventArgs e)
        {
            TextBox currentInputBox = sender as TextBox;
            TextBox mdp = txtbox_reg_mdp;
            var inputString = currentInputBox.Text;

            TimePSW.Enabled = true;
            TimePSW.Stop();
            TimePSW.Start();
            if (inputString.Length > currentPassword.Length)
            {
                var newChar = inputString.Substring(inputString.Length - 1);
                currentPassword += newChar;
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length - 1; i++)
                {
                    newBoxText += "*";
                }
                newBoxText += newChar;
                currentInputBox.Text = newBoxText;
                mdp.SelectionStart = mdp.Text.Count();
            }
            else if ((inputString.Length < currentPassword.Length) && (inputString.Length > 0)) // Removes characters from the currentPassword field til it matches the length of the textbox field and then exposes a single character at the end
            {
                currentPassword = currentPassword.Remove(inputString.Length, currentPassword.Length - inputString.Length);
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length; i++)
                {
                    newBoxText += "*";
                }
                currentInputBox.Text = newBoxText;
                mdp.SelectionStart = mdp.Text.Count();
            }
        }

        private void txtbox_confirmmdp_TextChanged(object sender, EventArgs e)
        {
            TextBox currentInputBox = sender as TextBox;
            TextBox mdp = txtbox_confirmmdp;
            var inputString = currentInputBox.Text;

            TimePSW2.Enabled = true;
            TimePSW2.Stop();
            TimePSW2.Start();
            if (inputString.Length > currentPassword.Length)
            {
                var newChar = inputString.Substring(inputString.Length - 1);
                currentPassword += newChar;
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length - 1; i++)
                {
                    newBoxText += "*";
                }
                newBoxText += newChar;
                currentInputBox.Text = newBoxText;
                mdp.SelectionStart = mdp.Text.Count();
            }
            else if ((inputString.Length < currentPassword.Length) && (inputString.Length > 0)) // Removes characters from the currentPassword field til it matches the length of the textbox field and then exposes a single character at the end
            {
                currentPassword = currentPassword.Remove(inputString.Length, currentPassword.Length - inputString.Length);
                var newBoxText = "";
                for (int i = 0; i < currentPassword.Length; i++)
                {
                    newBoxText += "*";
                }
                currentInputBox.Text = newBoxText;
                mdp.SelectionStart = mdp.Text.Count();
            }
        }

        private void TimePSW2_Tick(object sender, EventArgs e)
        {
            TextBox mdp = txtbox_confirmmdp;
            string text = "";
            for (int i = 0; i < mdp.Text.Count(); i++)
            {
                text = text + "*";
            }
            mdp.Text = text;
            TimePSW.Enabled = false;
            mdp.SelectionStart = mdp.Text.Count();
        }
    }
}