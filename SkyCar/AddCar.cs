﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SkyCar
{
    public partial class AddCar : Form
    {
        public AddCar()
        {
            InitializeComponent();
            btn_add_menu_del.Enabled = false;
            btn_add_menu_list.Enabled = false;
            btn_add_menu_next.Enabled = false;
            btn_add_menu_preview.Enabled = false;
        }

        private void Btn_add_del_Click(object sender, EventArgs e)
        {
            txtbox_add_Brand.Text = "";
            txtbox_add_Model.Text = "";
            txtbox_add_Power.Text = "";
            cmbBox_add_Gaz.Text = "";
            txtbox_add_number.Text = "";
            txtBox_add_Image.Text = "";
            picBox_add_image.Image = null;
        }

        private void Btn_add_menu_home_Click(object sender, EventArgs e)
        {
            this.Hide();
            frm_homePage frm_homePage = new frm_homePage();
            frm_homePage.ShowDialog();
            this.Close();
        }

        private void Btn_add_menu_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Txtbox_add_Power_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsControl(e.KeyChar) || !Char.IsNumber(e.KeyChar))
            {
                e.Handled = true; // Set l'evenement comme etant completement fini
                MessageBox.Show("Veuillez entrez uniquement des chiffres svp", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btn_add_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtbox_add_Brand.Text) || string.IsNullOrEmpty(cmbBox_add_Gaz.Text) || string.IsNullOrEmpty(txtbox_add_Model.Text) || string.IsNullOrEmpty(txtbox_add_Power.Text) || string.IsNullOrEmpty(txtbox_add_number.Text) || string.IsNullOrEmpty(txtBox_add_Image.Text))
                {
                    MessageBox.Show("Champ(s) Manquant", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string image = txtBox_add_Image.Text;
                    string brend = txtbox_add_Brand.Text;
                    string typefuel = cmbBox_add_Gaz.Text;
                    string model = txtbox_add_Model.Text;
                    string power = txtbox_add_Power.Text;
                    string nbChassis = txtbox_add_number.Text;
                    Car car = new Car(model, brend, typefuel, power, nbChassis, image);
                    List<Car> list = car.CarList(car);
                    CarManager carmanager = new CarManager();
                    carmanager.AddCar(list);
                    MessageBox.Show("La Voiture <" + brend + " - " + model + "> a été ajoutée avec succès !", "Voiture ajouté !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtbox_add_Brand.Text = "";
                    txtbox_add_Model.Text = "";
                    txtbox_add_Power.Text = "";
                    cmbBox_add_Gaz.Text = "";
                    txtbox_add_number.Text = "";
                    txtBox_add_Image.Text = "";
                    picBox_add_image.Image = null;
                }
            }
            catch (DbError)
            {
                MessageBox.Show("Du à un probléme avec notre serveur, l'ajout de voiture est temporairement désactivée", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TxtBox_add_Image_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog()
            {
                Title = "Selection d'image",

                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "fichiers image (*.jpg, *.png, *.bmp, *.gif)|*.BMP;*.JPG;*.GIF;*.PNG",
            };
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtBox_add_Image.Text = fbd.FileName;
                picBox_add_image.ImageLocation = fbd.FileName;
            }
        } 

        private void Txtbox_add_number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsControl(e.KeyChar) || !Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Veuillez entrez uniquement des chiffres svp", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}