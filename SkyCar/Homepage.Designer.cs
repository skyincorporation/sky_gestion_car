﻿namespace SkyCar
{
    partial class frm_homePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_homePage));
            this.lbl_home_name = new System.Windows.Forms.Label();
            this.park1 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbl_power6 = new System.Windows.Forms.Label();
            this.lbl_model6 = new System.Windows.Forms.Label();
            this.lbl_gaz6 = new System.Windows.Forms.Label();
            this.lbl_brand6 = new System.Windows.Forms.Label();
            this.picBox6 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl_model3 = new System.Windows.Forms.Label();
            this.lbl_power3 = new System.Windows.Forms.Label();
            this.lbl_gaz3 = new System.Windows.Forms.Label();
            this.lbl_brand3 = new System.Windows.Forms.Label();
            this.picBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lbl_power5 = new System.Windows.Forms.Label();
            this.lbl_model5 = new System.Windows.Forms.Label();
            this.lbl_gaz5 = new System.Windows.Forms.Label();
            this.lbl_brand5 = new System.Windows.Forms.Label();
            this.picBox5 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbl_power2 = new System.Windows.Forms.Label();
            this.lbl_model2 = new System.Windows.Forms.Label();
            this.lbl_gaz2 = new System.Windows.Forms.Label();
            this.lbl_brand2 = new System.Windows.Forms.Label();
            this.picBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbl_power4 = new System.Windows.Forms.Label();
            this.lbl_model4 = new System.Windows.Forms.Label();
            this.lbl_gaz4 = new System.Windows.Forms.Label();
            this.lbl_brand4 = new System.Windows.Forms.Label();
            this.picBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_power1 = new System.Windows.Forms.Label();
            this.lbl_model1 = new System.Windows.Forms.Label();
            this.lbl_gaz1 = new System.Windows.Forms.Label();
            this.lbl_brand1 = new System.Windows.Forms.Label();
            this.picBox_1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btn_home_del = new System.Windows.Forms.Button();
            this.btn_home_add = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_homepage_exit = new System.Windows.Forms.Button();
            this.park1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_home_name
            // 
            this.lbl_home_name.AutoSize = true;
            this.lbl_home_name.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl_home_name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_home_name.Location = new System.Drawing.Point(0, 471);
            this.lbl_home_name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_home_name.Name = "lbl_home_name";
            this.lbl_home_name.Size = new System.Drawing.Size(29, 13);
            this.lbl_home_name.TabIndex = 0;
            this.lbl_home_name.Text = "Nom";
            this.lbl_home_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // park1
            // 
            this.park1.BackColor = System.Drawing.Color.SteelBlue;
            this.park1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.park1.Controls.Add(this.groupBox7);
            this.park1.Controls.Add(this.groupBox4);
            this.park1.Controls.Add(this.groupBox6);
            this.park1.Controls.Add(this.groupBox3);
            this.park1.Controls.Add(this.groupBox5);
            this.park1.Controls.Add(this.groupBox2);
            this.park1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.park1.ForeColor = System.Drawing.SystemColors.Control;
            this.park1.Location = new System.Drawing.Point(167, 8);
            this.park1.Margin = new System.Windows.Forms.Padding(2);
            this.park1.Name = "park1";
            this.park1.Padding = new System.Windows.Forms.Padding(2);
            this.park1.Size = new System.Drawing.Size(523, 455);
            this.park1.TabIndex = 7;
            this.park1.TabStop = false;
            this.park1.Text = "Parque Voiture";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbl_power6);
            this.groupBox7.Controls.Add(this.lbl_model6);
            this.groupBox7.Controls.Add(this.lbl_gaz6);
            this.groupBox7.Controls.Add(this.lbl_brand6);
            this.groupBox7.Controls.Add(this.picBox6);
            this.groupBox7.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox7.Location = new System.Drawing.Point(347, 233);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(164, 209);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Voiture 6";
            this.groupBox7.Visible = false;
            // 
            // lbl_power6
            // 
            this.lbl_power6.AutoSize = true;
            this.lbl_power6.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power6.Location = new System.Drawing.Point(102, 180);
            this.lbl_power6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power6.Name = "lbl_power6";
            this.lbl_power6.Size = new System.Drawing.Size(58, 19);
            this.lbl_power6.TabIndex = 4;
            this.lbl_power6.Text = "label13";
            // 
            // lbl_model6
            // 
            this.lbl_model6.AutoSize = true;
            this.lbl_model6.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model6.Location = new System.Drawing.Point(86, 144);
            this.lbl_model6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model6.Name = "lbl_model6";
            this.lbl_model6.Size = new System.Drawing.Size(45, 16);
            this.lbl_model6.TabIndex = 3;
            this.lbl_model6.Text = "label14";
            // 
            // lbl_gaz6
            // 
            this.lbl_gaz6.AutoSize = true;
            this.lbl_gaz6.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz6.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz6.Name = "lbl_gaz6";
            this.lbl_gaz6.Size = new System.Drawing.Size(58, 19);
            this.lbl_gaz6.TabIndex = 2;
            this.lbl_gaz6.Text = "label15";
            // 
            // lbl_brand6
            // 
            this.lbl_brand6.AutoSize = true;
            this.lbl_brand6.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand6.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand6.Name = "lbl_brand6";
            this.lbl_brand6.Size = new System.Drawing.Size(58, 19);
            this.lbl_brand6.TabIndex = 1;
            this.lbl_brand6.Text = "label16";
            // 
            // picBox6
            // 
            this.picBox6.Location = new System.Drawing.Point(4, 16);
            this.picBox6.Margin = new System.Windows.Forms.Padding(2);
            this.picBox6.Name = "picBox6";
            this.picBox6.Size = new System.Drawing.Size(156, 116);
            this.picBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox6.TabIndex = 0;
            this.picBox6.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl_model3);
            this.groupBox4.Controls.Add(this.lbl_power3);
            this.groupBox4.Controls.Add(this.lbl_gaz3);
            this.groupBox4.Controls.Add(this.lbl_brand3);
            this.groupBox4.Controls.Add(this.picBox3);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Location = new System.Drawing.Point(347, 19);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(164, 209);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Voiture 3";
            this.groupBox4.Visible = false;
            // 
            // lbl_model3
            // 
            this.lbl_model3.AutoSize = true;
            this.lbl_model3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model3.Location = new System.Drawing.Point(92, 143);
            this.lbl_model3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model3.Name = "lbl_model3";
            this.lbl_model3.Size = new System.Drawing.Size(39, 16);
            this.lbl_model3.TabIndex = 5;
            this.lbl_model3.Text = "label1";
            this.lbl_model3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_power3
            // 
            this.lbl_power3.AutoSize = true;
            this.lbl_power3.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power3.Location = new System.Drawing.Point(110, 180);
            this.lbl_power3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power3.Name = "lbl_power3";
            this.lbl_power3.Size = new System.Drawing.Size(50, 19);
            this.lbl_power3.TabIndex = 4;
            this.lbl_power3.Text = "label9";
            this.lbl_power3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_gaz3
            // 
            this.lbl_gaz3.AutoSize = true;
            this.lbl_gaz3.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz3.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz3.Name = "lbl_gaz3";
            this.lbl_gaz3.Size = new System.Drawing.Size(58, 19);
            this.lbl_gaz3.TabIndex = 2;
            this.lbl_gaz3.Text = "label11";
            // 
            // lbl_brand3
            // 
            this.lbl_brand3.AutoSize = true;
            this.lbl_brand3.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand3.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand3.Name = "lbl_brand3";
            this.lbl_brand3.Size = new System.Drawing.Size(58, 19);
            this.lbl_brand3.TabIndex = 1;
            this.lbl_brand3.Text = "label12";
            // 
            // picBox3
            // 
            this.picBox3.Location = new System.Drawing.Point(4, 16);
            this.picBox3.Margin = new System.Windows.Forms.Padding(2);
            this.picBox3.Name = "picBox3";
            this.picBox3.Size = new System.Drawing.Size(156, 116);
            this.picBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox3.TabIndex = 0;
            this.picBox3.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lbl_power5);
            this.groupBox6.Controls.Add(this.lbl_model5);
            this.groupBox6.Controls.Add(this.lbl_gaz5);
            this.groupBox6.Controls.Add(this.lbl_brand5);
            this.groupBox6.Controls.Add(this.picBox5);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox6.Location = new System.Drawing.Point(179, 233);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(164, 209);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Voiture 5";
            this.groupBox6.Visible = false;
            // 
            // lbl_power5
            // 
            this.lbl_power5.AutoSize = true;
            this.lbl_power5.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power5.Location = new System.Drawing.Point(105, 180);
            this.lbl_power5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power5.Name = "lbl_power5";
            this.lbl_power5.Size = new System.Drawing.Size(58, 19);
            this.lbl_power5.TabIndex = 4;
            this.lbl_power5.Text = "label17";
            // 
            // lbl_model5
            // 
            this.lbl_model5.AutoSize = true;
            this.lbl_model5.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model5.Location = new System.Drawing.Point(93, 143);
            this.lbl_model5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model5.Name = "lbl_model5";
            this.lbl_model5.Size = new System.Drawing.Size(45, 16);
            this.lbl_model5.TabIndex = 3;
            this.lbl_model5.Text = "label18";
            // 
            // lbl_gaz5
            // 
            this.lbl_gaz5.AutoSize = true;
            this.lbl_gaz5.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz5.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz5.Name = "lbl_gaz5";
            this.lbl_gaz5.Size = new System.Drawing.Size(58, 19);
            this.lbl_gaz5.TabIndex = 2;
            this.lbl_gaz5.Text = "label19";
            // 
            // lbl_brand5
            // 
            this.lbl_brand5.AutoSize = true;
            this.lbl_brand5.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand5.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand5.Name = "lbl_brand5";
            this.lbl_brand5.Size = new System.Drawing.Size(58, 19);
            this.lbl_brand5.TabIndex = 1;
            this.lbl_brand5.Text = "label20";
            // 
            // picBox5
            // 
            this.picBox5.Location = new System.Drawing.Point(4, 16);
            this.picBox5.Margin = new System.Windows.Forms.Padding(2);
            this.picBox5.Name = "picBox5";
            this.picBox5.Size = new System.Drawing.Size(156, 116);
            this.picBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox5.TabIndex = 0;
            this.picBox5.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbl_power2);
            this.groupBox3.Controls.Add(this.lbl_model2);
            this.groupBox3.Controls.Add(this.lbl_gaz2);
            this.groupBox3.Controls.Add(this.lbl_brand2);
            this.groupBox3.Controls.Add(this.picBox2);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Location = new System.Drawing.Point(179, 19);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(164, 209);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Voiture 2";
            this.groupBox3.Visible = false;
            // 
            // lbl_power2
            // 
            this.lbl_power2.AutoSize = true;
            this.lbl_power2.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power2.Location = new System.Drawing.Point(111, 180);
            this.lbl_power2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power2.Name = "lbl_power2";
            this.lbl_power2.Size = new System.Drawing.Size(52, 19);
            this.lbl_power2.TabIndex = 4;
            this.lbl_power2.Text = "lblCV2";
            // 
            // lbl_model2
            // 
            this.lbl_model2.AutoSize = true;
            this.lbl_model2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model2.Location = new System.Drawing.Point(106, 141);
            this.lbl_model2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model2.Name = "lbl_model2";
            this.lbl_model2.Size = new System.Drawing.Size(32, 16);
            this.lbl_model2.TabIndex = 3;
            this.lbl_model2.Text = "lblM";
            // 
            // lbl_gaz2
            // 
            this.lbl_gaz2.AutoSize = true;
            this.lbl_gaz2.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz2.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz2.Name = "lbl_gaz2";
            this.lbl_gaz2.Size = new System.Drawing.Size(64, 19);
            this.lbl_gaz2.TabIndex = 2;
            this.lbl_gaz2.Text = "lbl_gaz2";
            // 
            // lbl_brand2
            // 
            this.lbl_brand2.AutoSize = true;
            this.lbl_brand2.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand2.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand2.Name = "lbl_brand2";
            this.lbl_brand2.Size = new System.Drawing.Size(95, 19);
            this.lbl_brand2.TabIndex = 1;
            this.lbl_brand2.Text = "label_brand1";
            // 
            // picBox2
            // 
            this.picBox2.Location = new System.Drawing.Point(4, 16);
            this.picBox2.Margin = new System.Windows.Forms.Padding(2);
            this.picBox2.Name = "picBox2";
            this.picBox2.Size = new System.Drawing.Size(156, 116);
            this.picBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox2.TabIndex = 0;
            this.picBox2.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbl_power4);
            this.groupBox5.Controls.Add(this.lbl_model4);
            this.groupBox5.Controls.Add(this.lbl_gaz4);
            this.groupBox5.Controls.Add(this.lbl_brand4);
            this.groupBox5.Controls.Add(this.picBox4);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox5.Location = new System.Drawing.Point(11, 233);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(164, 209);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Voiture 4";
            this.groupBox5.Visible = false;
            // 
            // lbl_power4
            // 
            this.lbl_power4.AutoSize = true;
            this.lbl_power4.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power4.Location = new System.Drawing.Point(106, 180);
            this.lbl_power4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power4.Name = "lbl_power4";
            this.lbl_power4.Size = new System.Drawing.Size(58, 19);
            this.lbl_power4.TabIndex = 4;
            this.lbl_power4.Text = "label21";
            // 
            // lbl_model4
            // 
            this.lbl_model4.AutoSize = true;
            this.lbl_model4.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model4.Location = new System.Drawing.Point(93, 141);
            this.lbl_model4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model4.Name = "lbl_model4";
            this.lbl_model4.Size = new System.Drawing.Size(45, 16);
            this.lbl_model4.TabIndex = 3;
            this.lbl_model4.Text = "label22";
            // 
            // lbl_gaz4
            // 
            this.lbl_gaz4.AutoSize = true;
            this.lbl_gaz4.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz4.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz4.Name = "lbl_gaz4";
            this.lbl_gaz4.Size = new System.Drawing.Size(58, 19);
            this.lbl_gaz4.TabIndex = 2;
            this.lbl_gaz4.Text = "label23";
            // 
            // lbl_brand4
            // 
            this.lbl_brand4.AutoSize = true;
            this.lbl_brand4.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand4.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand4.Name = "lbl_brand4";
            this.lbl_brand4.Size = new System.Drawing.Size(58, 19);
            this.lbl_brand4.TabIndex = 1;
            this.lbl_brand4.Text = "label24";
            // 
            // picBox4
            // 
            this.picBox4.Location = new System.Drawing.Point(4, 16);
            this.picBox4.Margin = new System.Windows.Forms.Padding(2);
            this.picBox4.Name = "picBox4";
            this.picBox4.Size = new System.Drawing.Size(156, 116);
            this.picBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox4.TabIndex = 0;
            this.picBox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.groupBox2.Controls.Add(this.lbl_power1);
            this.groupBox2.Controls.Add(this.lbl_model1);
            this.groupBox2.Controls.Add(this.lbl_gaz1);
            this.groupBox2.Controls.Add(this.lbl_brand1);
            this.groupBox2.Controls.Add(this.picBox_1);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Location = new System.Drawing.Point(11, 19);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(164, 209);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Voiture 1";
            this.groupBox2.Visible = false;
            // 
            // lbl_power1
            // 
            this.lbl_power1.AutoSize = true;
            this.lbl_power1.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power1.Location = new System.Drawing.Point(110, 180);
            this.lbl_power1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_power1.Name = "lbl_power1";
            this.lbl_power1.Size = new System.Drawing.Size(50, 19);
            this.lbl_power1.TabIndex = 4;
            this.lbl_power1.Text = "lbl_CV";
            // 
            // lbl_model1
            // 
            this.lbl_model1.AutoSize = true;
            this.lbl_model1.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_model1.Location = new System.Drawing.Point(103, 143);
            this.lbl_model1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_model1.Name = "lbl_model1";
            this.lbl_model1.Size = new System.Drawing.Size(35, 16);
            this.lbl_model1.TabIndex = 3;
            this.lbl_model1.Text = "LblM";
            // 
            // lbl_gaz1
            // 
            this.lbl_gaz1.AutoSize = true;
            this.lbl_gaz1.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gaz1.Location = new System.Drawing.Point(4, 180);
            this.lbl_gaz1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_gaz1.Name = "lbl_gaz1";
            this.lbl_gaz1.Size = new System.Drawing.Size(64, 19);
            this.lbl_gaz1.TabIndex = 2;
            this.lbl_gaz1.Text = "lbl_gaz1";
            // 
            // lbl_brand1
            // 
            this.lbl_brand1.AutoSize = true;
            this.lbl_brand1.Font = new System.Drawing.Font("Microsoft Tai Le", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_brand1.Location = new System.Drawing.Point(4, 141);
            this.lbl_brand1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_brand1.Name = "lbl_brand1";
            this.lbl_brand1.Size = new System.Drawing.Size(95, 19);
            this.lbl_brand1.TabIndex = 1;
            this.lbl_brand1.Text = "label_Brand1";
            // 
            // picBox_1
            // 
            this.picBox_1.Location = new System.Drawing.Point(4, 16);
            this.picBox_1.Margin = new System.Windows.Forms.Padding(2);
            this.picBox_1.Name = "picBox_1";
            this.picBox_1.Size = new System.Drawing.Size(156, 116);
            this.picBox_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox_1.TabIndex = 0;
            this.picBox_1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Location = new System.Drawing.Point(8, 334);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 29);
            this.button1.TabIndex = 5;
            this.button1.Text = "<----";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.Control;
            this.button3.Location = new System.Drawing.Point(8, 307);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 29);
            this.button3.TabIndex = 4;
            this.button3.Text = "---->";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightSkyBlue;
            this.button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button4.BackgroundImage")));
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.Control;
            this.button4.Location = new System.Drawing.Point(8, 240);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 63);
            this.button4.TabIndex = 3;
            this.button4.Text = "Liste simple";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // btn_home_del
            // 
            this.btn_home_del.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_home_del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_home_del.BackgroundImage")));
            this.btn_home_del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_home_del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_home_del.FlatAppearance.BorderSize = 0;
            this.btn_home_del.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_home_del.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_home_del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_home_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_home_del.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_home_del.Location = new System.Drawing.Point(8, 185);
            this.btn_home_del.Margin = new System.Windows.Forms.Padding(2);
            this.btn_home_del.Name = "btn_home_del";
            this.btn_home_del.Size = new System.Drawing.Size(133, 63);
            this.btn_home_del.TabIndex = 2;
            this.btn_home_del.Text = "Supprimer";
            this.btn_home_del.UseVisualStyleBackColor = false;
            // 
            // btn_home_add
            // 
            this.btn_home_add.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_home_add.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_home_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_home_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_home_add.FlatAppearance.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.btn_home_add.FlatAppearance.BorderSize = 0;
            this.btn_home_add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_home_add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_home_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_home_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_home_add.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_home_add.Location = new System.Drawing.Point(8, 133);
            this.btn_home_add.Margin = new System.Windows.Forms.Padding(2);
            this.btn_home_add.Name = "btn_home_add";
            this.btn_home_add.Size = new System.Drawing.Size(133, 63);
            this.btn_home_add.TabIndex = 1;
            this.btn_home_add.Text = "Ajouter";
            this.btn_home_add.UseVisualStyleBackColor = false;
            this.btn_home_add.Click += new System.EventHandler(this.Btn_home_add_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SkyCar.Properties.Resources._1200px_SKY_Basic_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(8, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 107);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // btn_homepage_exit
            // 
            this.btn_homepage_exit.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_homepage_exit.BackgroundImage = global::SkyCar.Properties.Resources.Skycar_btn;
            this.btn_homepage_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_homepage_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_homepage_exit.FlatAppearance.BorderSize = 0;
            this.btn_homepage_exit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_homepage_exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_homepage_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_homepage_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_homepage_exit.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_homepage_exit.Location = new System.Drawing.Point(8, 382);
            this.btn_homepage_exit.Margin = new System.Windows.Forms.Padding(2);
            this.btn_homepage_exit.Name = "btn_homepage_exit";
            this.btn_homepage_exit.Size = new System.Drawing.Size(133, 40);
            this.btn_homepage_exit.TabIndex = 6;
            this.btn_homepage_exit.Text = "Quitter";
            this.btn_homepage_exit.UseVisualStyleBackColor = false;
            this.btn_homepage_exit.Click += new System.EventHandler(this.Btn_homepage_exit_Click);
            // 
            // frm_homePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(727, 484);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.park1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btn_home_del);
            this.Controls.Add(this.btn_home_add);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_homepage_exit);
            this.Controls.Add(this.lbl_home_name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frm_homePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Homepage";
            this.Load += new System.EventHandler(this.frm_homePage_Load);
            this.park1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_home_name;
        private System.Windows.Forms.Button btn_homepage_exit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_home_add;
        private System.Windows.Forms.Button btn_home_del;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox park1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbl_power6;
        private System.Windows.Forms.Label lbl_model6;
        private System.Windows.Forms.Label lbl_gaz6;
        private System.Windows.Forms.Label lbl_brand6;
        private System.Windows.Forms.PictureBox picBox6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lbl_power3;
        private System.Windows.Forms.Label lbl_gaz3;
        private System.Windows.Forms.Label lbl_brand3;
        private System.Windows.Forms.PictureBox picBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lbl_power5;
        private System.Windows.Forms.Label lbl_model5;
        private System.Windows.Forms.Label lbl_gaz5;
        private System.Windows.Forms.Label lbl_brand5;
        private System.Windows.Forms.PictureBox picBox5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_power2;
        private System.Windows.Forms.Label lbl_model2;
        private System.Windows.Forms.Label lbl_gaz2;
        private System.Windows.Forms.Label lbl_brand2;
        private System.Windows.Forms.PictureBox picBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lbl_power4;
        private System.Windows.Forms.Label lbl_model4;
        private System.Windows.Forms.Label lbl_gaz4;
        private System.Windows.Forms.Label lbl_brand4;
        private System.Windows.Forms.PictureBox picBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lbl_power1;
        private System.Windows.Forms.Label lbl_model1;
        private System.Windows.Forms.Label lbl_gaz1;
        private System.Windows.Forms.Label lbl_brand1;
        private System.Windows.Forms.PictureBox picBox_1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbl_model3;
    }
}