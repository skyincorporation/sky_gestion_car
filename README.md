# Description du projet

SkyCar est une application pour la gestion de stock des voitures disponible dans vos garages.

## Instalation

Afin de déployer votre projet sur votre machine local.
* 1. [Suivez le lien OneDrive.](https://cpnv1-my.sharepoint.com/:f:/r/personal/nicolas_glassey_cpnv_ch/Documents/CPNV/ProjetCSharp_2019_2020_Deliveries/ClasseA/NGY_SKY_GESTION_CAR?csf=1&e=HsaVoW)
* 2. Téléchargé en local le .exe de notre application.
* 3. Lancer le script SQL afin de créer la base de donnée.

### Lancement

* 1. Lancer le .exe
* 2. Enregistrez-vous ou Logez-vous avec l'utilisateur suivant (Username : test@test.ch / Password : test).

## Lancement des tests

* Les test passent tous avec succès.

## Versioning

Vous pouvez consultez tous le suivis du projet sur les commit du BitBucket.

## Participants

* **Szymon Jagla**
* **Loïc Gavin**
