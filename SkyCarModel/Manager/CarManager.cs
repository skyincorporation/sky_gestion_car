﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SkyCar
{
    public class CarManager
    {
        public CarManager()
        {
        }

        public List<Car> SelectCar()
        {
            List<string> listCar = null;
            string tempread = "";
            MySqlCommand command;
            MySqlDataReader dataReader;
            DbConnector cnn = new DbConnector();
            MySqlConnection DbOpen = cnn.DbConnect();

            try
            {
                command = new MySqlCommand("SELECT Idvehicle,Model,Brend,Power,Typefuel,NbChassis,Picture FROM vehicles", DbOpen);

                try
                {
                    dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        // iterate your results here
                        string read = String.Format("{0}", dataReader["Idvehicle"]) + ";" + String.Format("{0}", dataReader["Model"]) + ";" + String.Format("{0}", dataReader["Brend"]) + ";" + String.Format("{0}", dataReader["Power"]) + ";" + String.Format("{0}", dataReader["Typefuel"]) + ";" + String.Format("{0}", dataReader["NbChassis"]) + ";" + String.Format("{0}", dataReader["Picture"]) + ",";

                        tempread = tempread + read;

                        listCar = tempread.Split(',').ToList();
                    }

                    listCar.Remove("");
                    command.Dispose();
                    List<Car> carList = Split(listCar);
                    return carList;
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    throw new DbError();
                }
            }
            catch (System.InvalidOperationException)
            {
                cnn.DbClose(DbOpen);
                throw new Exception("Errreur de Commande SQL");
            }
        }

        public List<Car> SelectCar(int nbChassis)
        {
            List<string> listCar = null;
            string tempread = "";
            MySqlCommand command;
            MySqlDataReader dataReader;
            DbConnector cnn = new DbConnector();
            MySqlConnection DbOpen = cnn.DbConnect();

            try
            {
                command = new MySqlCommand("SELECT Idvehicle,Model,Brend,Power,Typefuel,NbChassis,Picture FROM vehicles WHERE NbChassis = @NbChassis;", DbOpen);
                command.Parameters.AddWithValue("@NbChassis", nbChassis);

                try
                {
                    dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        // iterate your results here
                        string read = String.Format("{0}", dataReader["Idvehicle"]) + ";" + String.Format("{0}", dataReader["Model"]) + ";" + String.Format("{0}", dataReader["Brend"]) + ";" + String.Format("{0}", dataReader["Power"]) + ";" + String.Format("{0}", dataReader["Typefuel"]) + ";" + String.Format("{0}", dataReader["NbChassis"]) + ";" + String.Format("{0}", dataReader["Picture"]) + ",";

                        tempread = tempread + read;

                        listCar = tempread.Split(',').ToList();
                    }

                    listCar.Remove("");
                    command.Dispose();
                    List<Car> carList = Split(listCar);
                    return carList;
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    throw new DbError();
                }
            }
            catch (System.InvalidOperationException)
            {
                cnn.DbClose(DbOpen);
                throw new Exception("Errreur de Commande SQL");
            }
        }

        public bool AddCar(List<Car> carList)
        {
            bool result = false;
            MySqlCommand command;
            DbConnector cnn = new DbConnector();
            MySqlConnection DbOpen = cnn.DbConnect();

            try
            {
                command = new MySqlCommand("INSERT INTO vehicles (Model, Brend, Power, Typefuel, NbChassis, Picture) VALUE (@Model, @Brend, @Power, @Typefuel, @NbChassis, @Picture)", DbOpen);
                command.Parameters.AddWithValue("@Model", carList[0].Model);
                command.Parameters.AddWithValue("@Brend", carList[0].Brend);
                command.Parameters.AddWithValue("@Power", carList[0].Power);
                command.Parameters.AddWithValue("@Typefuel", carList[0].Typefuel);
                command.Parameters.AddWithValue("@NbChassis", carList[0].NbChassis);
                command.Parameters.AddWithValue("@Picture", carList[0].Picture);

                try
                {
                    int RowsAffected = command.ExecuteNonQuery();
                    if (RowsAffected == 1)
                    {
                        result = true;
                    }
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    return result;
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    throw new DbError();
                }
            }
            catch (System.InvalidOperationException)
            {
                cnn.DbClose(DbOpen);
                throw new Exception("Errreur de Commande SQL");
            }
        }

        public List<Car> Split(List<string> listcar)
        {
            List<Car> carlist = new List<Car>();

            foreach (string list in listcar)
            {
                Car car = new Car(list);
                carlist.Add(car);
            }

            return carlist;
        }
    }
}