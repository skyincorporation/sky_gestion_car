﻿using MySql.Data.MySqlClient;
using System;
using System.Security.Cryptography;
using System.Text;

namespace SkyCar
{
    public class UserManager
    {
        public bool IsLoginCorrect(string userEmailAddress, string userPsw)
        {
            bool result = false;
            string userHashPsw = "";
            MySqlCommand command;
            MySqlDataReader dataReader;
            DbConnector cnn = new DbConnector();
            MySqlConnection DbOpen = cnn.DbConnect();

            try
            {
                command = new MySqlCommand("SELECT userHashPsw FROM users WHERE userEmailAddress = @userEmailAddress", DbOpen);
                command.Parameters.AddWithValue("@userEmailAddress", userEmailAddress);

                try
                {
                    dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        userHashPsw = userHashPsw + dataReader.GetValue(0);
                    }

                    string hashedPassword = ComputeSha256Hash(userPsw);

                    if (userHashPsw == hashedPassword)
                    {
                        result = true;
                    }

                    command.Dispose();
                    return result;
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    throw new DbError();
                }
            }
            catch (System.InvalidOperationException)
            {
                cnn.DbClose(DbOpen);
                throw new Exception("Errreur de Commande SQL");
            }
        }

        public bool RegisterNewAccount(string userEmailAddress, string userPsw)
        {
            bool result = false;
            MySqlCommand command;
            DbConnector cnn = new DbConnector();
            MySqlConnection DbOpen = cnn.DbConnect();
            string userHashPsw = ComputeSha256Hash(userPsw);

            try
            {
                command = new MySqlCommand("INSERT INTO users (userEmailAddress, userHashPsw) VALUES (@userEmailAddress, @userHashPsw)", DbOpen);
                command.Parameters.AddWithValue("@userEmailAddress", userEmailAddress);
                command.Parameters.AddWithValue("@userHashPsw", userHashPsw);

                try
                {
                    int RowsAffected = command.ExecuteNonQuery();
                    if (RowsAffected == 1)
                    {
                        result = true;
                    }
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    return result;
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    command.Dispose();
                    cnn.DbClose(DbOpen);
                    throw new DbError();
                }
            }
            catch (System.InvalidOperationException)
            {
                cnn.DbClose(DbOpen);
                throw new Exception("Errreur de Commande SQL");
            }
        }

        private static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}