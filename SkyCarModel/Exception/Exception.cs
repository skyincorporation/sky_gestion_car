﻿using System;

namespace SkyCar
{
    public class DbError : Exception
    {
        public DbError()
            : base("La base de donnée n'est pas opérationelle")
        { }
    }
}