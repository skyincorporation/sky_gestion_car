﻿namespace SkyCar
{
    public class UserSettings
    {
        private int positionX, positionY, sizeX, sizeY;

        public UserSettings()
        {
        }

        public UserSettings(int positionX, int positionY, int sizeX, int sizeY)
        {
            this.positionX = positionX;
            this.positionY = positionY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }

        public int PositionX { get => positionX; set => positionX = value; }
        public int PositionY { get => positionY; set => positionY = value; }
        public int Sizey { get => sizeY; set => sizeY = value; }
        public int SizeX { get => sizeX; set => sizeX = value; }
    }
}