﻿namespace SkyCar
{
    public static class Session
    {
        private static string email;
        private static UserSettings settings;

        public static string Email { get => email; set => email = value; }
        public static UserSettings Settings { get => settings; set => settings = value; }

        public static void UpdateSettings()
        {
            JsonConnector jsonConnector = new JsonConnector();
            settings = jsonConnector.Read(email);
        }

        public static void SaveSettings()
        {
            JsonConnector.Write(email, settings);
        }
    }
}