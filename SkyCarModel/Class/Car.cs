﻿using System.Collections.Generic;

namespace SkyCar
{
    public class Car
    {
        private string model;
        private string brend;
        private string typefuel;
        private string power;
        private string nbChassis;
        private string picture;

        public Car()
        {
        }

        public Car(string listCar)
        {
            string[] listCarSplit = listCar.Split(';');

            this.model = listCarSplit[1];
            this.brend = listCarSplit[2];
            this.power = listCarSplit[3];
            this.typefuel = listCarSplit[4];
            this.nbChassis = listCarSplit[5];
            this.picture = listCarSplit[6];
        }

        public Car(string model, string brend, string typefuel, string power, string nbChassis, string picture)
        {
            this.model = model;
            this.brend = brend;
            this.typefuel = typefuel;
            this.power = power;
            this.nbChassis = nbChassis;
            this.picture = picture;
        }

        public string NbChassis { get => nbChassis; }
        public string Model { get => model; }
        public string Brend { get => brend; }
        public string Typefuel { get => typefuel; }
        public string Power { get => power; }
        public string Picture { get => picture; }

        public List<Car> CarList(Car car)
        {
            List<Car> carList = new List<Car>();
            carList.Add(car);
            return carList;
        }
    }
}