﻿using MySql.Data.MySqlClient;
using System;

namespace SkyCar
{
    public class DbConnector
    {
        public MySqlConnection DbConnect()
        {
            string connetionString;
            connetionString = @"Data Source=localhost;Initial Catalog=skycar;User ID=CarManager;Password=Manage";
            MySqlConnection cnn = new MySqlConnection(connetionString);

            try
            {
                cnn.Open();
                return cnn;
            }
            catch (MySql.Data.MySqlClient.MySqlException)
            {
                throw new Exception("Connection à la base de donnée impossible");
            }
        }

        public void DbClose(MySqlConnection cnn)
        {
            cnn.Close();
        }
    }
}