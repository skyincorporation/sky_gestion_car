﻿using Newtonsoft.Json;
using System.IO;

namespace SkyCar
{
    public class JsonConnector
    {
        private const string path = @"";

        public UserSettings Read(string email)
        {
            UserSettings settingsFromJson;

            if (File.Exists(path + email + ".json"))
            {
                string jsonFromFile;
                using (var reader = new StreamReader(path + email + ".json"))
                {
                    jsonFromFile = reader.ReadToEnd();
                }
                settingsFromJson = JsonConvert.DeserializeObject<UserSettings>(jsonFromFile);
            }
            else
            {
                settingsFromJson = new UserSettings(0, 0, 641, 466);
            }

            return settingsFromJson;
        }

        public static void Write(string email, UserSettings settings)
        {
            string output = JsonConvert.SerializeObject(settings);
            using (StreamWriter writer = new StreamWriter(path + email + ".json"))
            {
                writer.WriteLine(output);
            }
        }
    }
}